package com.epam.rd.autotasks;

class CycleSwap {
    static void cycleSwap(int[] array) {

        if (array.length == 0) {
            return;
        }

        for (int i = 0; i < 1; i++) {
            int lastNumber;
            lastNumber = array[array.length - 1];

            for (int j = array.length - 1; j > 0; j--) {
                //Shift elements
                array[j] = array[j - 1];
            }
            //Add last element to the beginning
            array[0] = lastNumber;
        }
    }

    static void cycleSwap(int[] array, int shift) {

        if (array.length == 0) {
            return;
        }
        for (int i = 0; i < shift; i++) {
            int lastNumber;
            lastNumber = array[array.length - 1];

            for (int j = array.length - 1; j > 0; j--) {
                array[j] = array[j - 1];
            }
            array[0] = lastNumber;
        }
    }
}
